
# Project 21-MATF-Planer-Preklapač

Aplikacija koja pomaže studentima (sa matematičkog fakulteta) da organizuju svoje aktivnosti na fakultetu (raspored časova, datume kolokvijuma,ispita, predispitne obaveze itd.)

## Developers

- [Aleksandra Radosavljevic, 472/2018](https://gitlab.com/astr0nom1ja)
- [Milena Kurtic, 427/2016](https://gitlab.com/MilenaKurtic)
- [Pavle Brajkovic, 498/2018](https://gitlab.com/pavlebrajkovic)
- [Marija Filipovic, 482/2017](https://gitlab.com/MarijaFilipovic)
- [nan, nan](https://gitlab.com/nan)
