const express = require('express');
const router = express.Router();


const controllerIspit = require('../../controllers/ispitController');

router.get('/naziv', controllerIspit.getIspitByName);
router.get('/', controllerIspit.getIspiti);
router.post('/', controllerIspit.createIspit);
router.put('/',controllerIspit.izmeniUnos);
router.delete('/',controllerIspit.deleteIspit);


module.exports =router;