const express = require('express');
const router = express.Router();

const examGradeController = require("../../controllers/examGradeController");

//http://localhost:3004/examGrade/
router.post('/new_exam', examGradeController.createExam);
router.get('/', examGradeController.getExams);
router.post('/update_exam', examGradeController.updateByExamId);
router.delete('/delete_exam', examGradeController.deleteByExamId);

module.exports = router;