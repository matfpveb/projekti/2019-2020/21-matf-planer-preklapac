
const mongoose = require('mongoose');
const Predmet = require('../models/predmetModel.js');

const rasporedSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  indeks: {
    type: String,
    required: true        
  },
  predmeti: [{
    ime: {
      type: String,
      required: true,
    },
    ucionica: {
       type: String,
       require: true
    },
    termin: {
        type: String,
        require: true
    }
  }]
});


module.exports = mongoose.model('raspored', rasporedSchema);