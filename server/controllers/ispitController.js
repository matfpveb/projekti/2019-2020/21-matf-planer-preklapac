const mongoose = require('mongoose');
const Ispit = require('../models/ispitModel');
const User = require('../models/usersModel');

const http = require('http');
const cheerio = require('cheerio');
const cheerioTableparser = require('cheerio-tableparser');

module.exports.createIspit = async (req, res, next) => {
    if (!req.body.naziv || !req.body.datum || !req.body.vreme) {
        res.status(400);
        res.send();
      } else {
          try{
              const currentUser = await User.findOne({"indeks": req.body.indeks}).exec();
              if(currentUser)
              {
                  const newIspit = new Ispit({
                      _id:new mongoose.Types.ObjectId(),
                      naziv: req.body.naziv,
                      datum: req.body.datum,
                      vreme: req.body.vreme,
                      indeks: currentUser.indeks
                  });

                  await newIspit.save();
                  console.log("Cuvanje ispita");
                  res.status(201).json(newIspit);
              }
              else{
                  res.status(400);
              }
            }
            catch (err) {
               next(err);
              }
        }      
}

module.exports.getIspitByName = async (req, res, next) => {
    
    try{
        const currentUser = await User.findOne({"indeks": req.query.indeks}).exec();
        if(currentUser)
        {
        const ispit = await Ispit.find({"naziv": req.query.naziv},{"datum":req.query.datum}).exec();
        res.status(200).json(ispit);
        }
        else{
            res.status(400);
        }
        
    } catch (err) {
      next(err);
    }
  };

  module.exports.getIspiti = async (req, res, next) => {
    try{
        const currentUser = await User.findOne({"indeks": req.query.indeks}).exec();
        if(currentUser)
        {
        
            const ispit = await Ispit.find({"indeks":req.query.indeks}).exec();
            res.status(200).json(ispit);
        }
        else{
            res.status(400);
        }   
    } catch (err) {
      next(err);
    }

  };

  module.exports.deleteIspit = async (req,res,next) => {
     
    try{
        const currentUser = await User.findOne({"indeks": req.query.indeks}).exec();
        if(currentUser)
        {
            const ispit = await Ispit.find({"_id":req.query._id}).exec();
            if(ispit)
            {
                await Ispit.findOneAndDelete({"_id":req.query._id}).exec();
                res.status(200).send();
            }
            else{
                res.status(400).send();
            }
        }
        else{
            res.status(404).send();
        }   
    } catch (err) {
      next(err);
    }
  };

  module.exports.izmeniUnos = async(req,res,next)=> {
     
    try{
        const currentUser = await User.findOne({"indeks": req.query.indeks}).exec();
        if(currentUser)
        {
            const ispit = await Ispit.find({"_id":req.query._id}).exec();
            if(ispit)
            {
                const updateOptions = {};
                for (let i = 0; i < req.body.length; ++i) {
                    let option = req.body[i];
                    updateOptions[option.nazivPolja] = option.novaVrednost;
                }

                try {
                    await Ispit.updateOne({ _id: req.query._id }, { $set: {datum:req.body.datum} }).exec();
                    res.status(200).json({ message: 'The product is successfully updated' });
                  } catch (err) {
                    next(err);
                  }
            }
            else{
                res.status(404).send();
            }
        }
        else{
            res.status(404).send();
        }   
    } catch (err) {
      next(err);
    }
  };
