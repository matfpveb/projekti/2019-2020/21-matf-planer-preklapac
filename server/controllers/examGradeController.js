const mongoose = require('mongoose');
const ExamGrade = require('../models/examGradeModel');
const User = require('../models/usersModel');


module.exports.createExam = async (req, res, next) => {
    /**Kad se pravi nova kartica mora da ima bar predispitne poene */
    if (!req.body.subject  || !req.body.index || !req.body.testPoints) {
      res.status(400);
      res.send();
    } else {
      try {
        const currentUser = await User.findOne({"indeks": req.body.index}).exec();
    
        if(currentUser){
          const newExam = new ExamGrade({
            _id: new mongoose.Types.ObjectId(),
            subject: req.body.subject,
            testPoints: req.body.testPoints,
            examPoints: req.body.examPoints,
            user: currentUser._id
          });
          
          if(req.body.examPoints !== undefined && req.body.testPoints !== undefined){
            const grade = req.body.examPoints + req.body.testPoints;
            
            switch(true){
              case (grade < 51):
                newExam.grade = 5;
                break;
              case (grade >= 51 && grade < 61):
                newExam.grade = 6;
                break;
              case (grade >= 61 && grade < 71):
                newExam.grade = 7;
                break;
              case (grade >= 71 && grade < 81):
                newExam.grade = 8;
                break;
              case (grade >= 81 && grade < 91):
                newExam.grade = 9;
                break;
              case (grade >= 91):
                newExam.grade = 10;
                break;
            }
          } 

          await newExam.save();
    
          res.status(201).json(newExam);
        } else {
          res.status(400);
          res.send();
        }
        
      } catch (err) {
        next(err);
      }
    }
};


module.exports.getExams = async (req, res, next) => {
    if(!req.query.index){
      res.status(400);
      res.send();
    }
    try {

        const currentUser = await User.findOne({"indeks": req.query.index}).exec();
        
        if(currentUser){
            const notes = await ExamGrade.find({"user": currentUser._id}).exec();
            
            res.status(200).json(notes);
        } else {
            
            res.status(400);
            res.send();
        }
    } catch (err) {
      next(err);
    }    
};

module.exports.updateByExamId = async (req, res, next) => {
  if (!req.body.exam_id || !req.body.index) {
    console.log("Fali index ili id ispita");
    res.status(400);
    res.send();
  } else {
    try {
        const currentUser = await User.findOne({"indeks": req.body.index});
        const currentExam = await ExamGrade.findOne({"_id": req.body.exam_id, "user": currentUser._id});
        if(currentExam && currentUser){
      
          if(req.body.testPoints)
            currentExam.testPoints = req.body.testPoints;
          if(req.body.examPoints)
            currentExam.examPoints = req.body.examPoints;

            if(currentExam.examPoints !== undefined && currentExam.testPoints !== undefined){
              const grade = currentExam.examPoints + currentExam.testPoints;
              
              switch(true){
                case (grade < 51):
                  currentExam.grade = 5;
                  break;
                case (grade >= 51 && grade < 61):
                  currentExam.grade = 6;
                  break;
                case (grade >= 61 && grade < 71):
                  currentExam.grade = 7;
                  break;
                case (grade >= 71 && grade < 81):
                  currentExam.grade = 8;
                  break;
                case (grade >= 81 && grade < 91):
                  currentExam.grade = 9;
                  break;
                case (grade >= 91):
                  currentExam.grade = 10;
                  break;
              }
            } 
          

          await currentExam.save();
  
          res.status(200).json(currentExam);      
      } else {
        console.log("Nije pronadjen ispit")
        res.status(400);
        res.send();
      }
      
    } catch (err) {
      next(err);
    }
  }
};

module.exports.deleteByExamId = async function (req, res, next) {
  if(!req.query.exam_id){
    res.status(400);
    res.send();
  }
  try {
    await ExamGrade.deleteOne({ "_id": req.query.exam_id }).exec();
    
    res.status(200).json({ message: 'The exam is successfully deleted' });
  } catch (err) {
    next(err);
  }
};