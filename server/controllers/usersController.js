const mongoose = require('mongoose');
const Student = require('../models/usersModel');
const bcrypt = require('bcryptjs');


module.exports.getStudents = async (req, res, next) => {
  try {

    const stud = await Student.find({}).exec();
    res.status(200).json(stud);
    
  } catch (err) {
    next(err);
  }
};

module.exports.createStudent = async (req, res, next) => {
    if (!req.body.indeks || !req.body.lozinka) {
      res.status(400);
      res.send();
    } else {
      try {

        const pomStud = await Student.find({indeks: req.body.indeks}).exec();
        if(!pomStud.length){
          if(!req.body.ime || !req.body.prezime){
            res.status(400);
            res.send();
          } else {

            bcrypt.genSalt(10, function(err, salt){
              bcrypt.hash(req.body.lozinka, salt, function(err, hash){
                const newStud = new Student({
                  _id: new mongoose.Types.ObjectId(),
                  indeks: req.body.indeks,
                  lozinka: hash,
                  ime: req.body.ime,
                  prezime: req.body.prezime
                });
                
                //morala sam da obrisem await
                newStud.save();
          
                res.status(201).json(newStud);
              });
            });
            
          }
        }else{
          console.log("Korisnik sa tim indeksom vec postoji.")
          res.status(400);
          res.send();
        }
        
      } catch (err) {
        next(err);
      }
    }
  };

module.exports.getStudentByIndex = async (req, res, next) => {
  if(!req.body.indeks || !req.body.lozinka){
    res.status(400);
    console.log("Nema indeksa ili lozinke");
    res.send();
  } else {
    try {

      const stud = await Student.findOne({"indeks": req.body.indeks}).exec();
      bcrypt.compare(req.body.lozinka, stud.lozinka, function(err, result){
        if(result){
          res.status(200).json(stud);
        } else {
          console.log("Nisu iste lozinke");
          res.status(400).send();
        }
      });
      
      
    } catch (err) {
      next(err);
    }
  }
};

module.exports.changePasswordByIndex = async (req, res, next) => {
  if(!req.body.indeks || !req.body.lozinka || !req.body.novaLozinka){
    res.status(400);
    console.log("Nema indeksa, nove ili stare lozinke");
    res.send();
  } else {
    try {
      const stud = await Student.findOne({"indeks": req.body.indeks}).exec();
      bcrypt.compare(req.body.lozinka, stud.lozinka, function(err, result){
        if(result){
          bcrypt.genSalt(10, function(err, salt){
            bcrypt.hash(req.body.novaLozinka, salt, function(err, hash){
              stud.lozinka = hash;
              
              //morala sam da obrisem await
              stud.save();
        
              res.status(200).json();
            });
          });
          
        } else {
          console.log("Pogresna lozinka");
          res.status(400).send();
        }
      });
      
      
    } catch (err) {
      next(err);
    }
  }
};

module.exports.deleteAccount = async (req, res, next) => {
  if(!req.query.indeks){
    res.status(400);
    console.log("Nema indeksa!");
    res.send();
  } else {
    try {
      await Student.deleteOne({ "indeks": req.query.indeks }).exec();
      
      res.status(200).json({ message: 'The user is successfully deleted' });
    } catch (err) {
      next(err);
    }
  }
}