import { Component, OnInit, OnDestroy } from '@angular/core';
import { Predmet } from './../models/predmet.model';

import { PredmetService } from './../services/predmet.service';
import { Subscription, Observable } from 'rxjs';

import { PredmetNiz } from '../models/predmetniz.model';
import { RememberIndexService } from '../services/remember-index.service';

@Component({
  selector: 'app-raspored',
  templateUrl: './raspored.component.html',
  styleUrls: ['./raspored.component.css']
})
export class RasporedComponent implements OnInit {

  public raspored : PredmetNiz;
  public predmeti : Predmet[] = [];
  public predmet: string;
  public grupa: string;
  public indeksbezcrte: string;
  
  public indeksi: string[];
  public indeks: string; // povezati indeks sa logovanja i ukinuti ga i u servisu
  private activesubs: Subscription[] = [] ;

  
  
    
 
  constructor(
              private predmetService: PredmetService
              ) {
    
    
    
    
    
    if(localStorage.getItem('isLoggedIn') === "true"){
      this.indeks = JSON.parse(localStorage.getItem('currentUser')).indeks;
      this.indeksi = this.indeks.split("/");
      this.indeksbezcrte = this.indeksi[0] + this.indeksi[1];
    }
    

                

    const sub = this.predmetService.getPredmeti(this.indeksbezcrte)
    .subscribe((raspored: PredmetNiz) => {
      this.raspored = raspored;
      if(this.raspored[0] !== undefined){
        this.predmeti = this.raspored[0].predmeti; }
      
    
      this.unesiPredmete(this.predmeti);
    });
    this.activesubs.push(sub);



    // dovodi predmete iz baze u JSONU
    // deserijalizuje u niz predmeti i poziva unesi predmete

    
  }

  // unesiPredmete  {prodji kroz niz predmeti citaj iz njih termine i na ta mesta u tabeli upisi ostalo}.
  public unesiPredmete(predmeti :Predmet[]) {

    predmeti.forEach(element => {

      let vreme :string = element.termin;
      let polje = document.getElementById(vreme);
      polje.textContent = polje.textContent + element.ime + ", " +element.ucionica + " \n"
      
    });
    
   }


  // dodajPredmet {odaberi grupu i predmet -> salje zahtev serveru da ode na sajt faksa, pokupi html, izracuna 
  // kada i gde se predmet drzi i zatim vraca Predmet[]
  public dodajPredmet() {

    
    const sub1 = this.predmetService.dodajPredmet(this.grupa, this.predmet)
    .subscribe((dodati: PredmetNiz) => {
      this.predmeti.forEach(p=> {
        let polje = document.getElementById(p.termin);
        polje.textContent=""; }); // resetuje tabelu pred novi unos
      if (!(dodati == null))
      {this.predmeti = this.predmeti.concat(dodati.predmeti);}
      this.unesiPredmete(this.predmeti);


    });
    this.activesubs.push(sub1);

       

   }


  // izbaciPredmet {odaberi predmet -> izbacuje iz predmeti sve termine tog predmeta i poziva unesiPredmete}
  public izbaciPredmet()
     {
          this.predmeti.map(p=>{
          let vreme :string = p.termin;
          let polje = document.getElementById(vreme);
          polje.textContent = "";})
          this.predmeti = this.predmeti.filter(p=>p.ime!==this.predmet)
          this.unesiPredmete(this.predmeti);
          console.log(this.predmet + " " + this.grupa)

     }
  public sacuvaj(){
  // sacuvaj {salje u bazu niz predmeti serijalizovan u  JSON}
     const sub2 =  this.predmetService.sacuvajRaspored(this.predmeti, this.indeksbezcrte)
     .subscribe((raspored ) => {
       window.alert("Sacuvao si raspored!");
     });
     this.activesubs.push(sub2);
     }

 


  ngOnInit(): void {
   
     // unosi predmete dohvacene pri inicijalizaciji
  }
  ngOnDestroy(): void{
    this.activesubs.forEach(sub => sub.unsubscribe());
  }
}
