import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { User } from '../models/user';
import { LoginRegisterService } from '../services/login-register-service.service';
import { InformationDialogComponent } from './../dialogs/information-dialog/information-dialog.component';


@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.css']
})
export class MyAccountComponent implements OnInit {
  private user: User;
  public pass_change_el: boolean;
  public deleteAccountWanted: boolean;
  public deleteIsConfirmed: boolean;

  private passEntry;
  private newPassEntry1;
  private newPassEntry2;
  

  constructor(private router: Router, private userService: LoginRegisterService,  protected dialog: MatDialog) { 
    if(localStorage.getItem('isLoggedIn') === "true"){
      this.user = JSON.parse(localStorage.getItem('currentUser'));    
      this.pass_change_el = false; 
      this.deleteAccountWanted = false;
      this.deleteIsConfirmed = false;
    }
  }

  public get ime(): string{
    return this.user.ime;
  }

  public get prezime(): string{
    return this.user.prezime;
  }

  public get indeks(): string{
    return this.user.indeks;
  }

  public get lozinka(): string{
    /**Ovde zapravo vracam samo nisku sa zvezdicama ali je duzine korisnikove lozinke */
    var result = "";
    for ( var i = 0; i < this.user.lozinka.length; i++ ) {
       result += '*';
    }
    return result;
  }

  ngOnInit(): void {
  }

  changeUserPassword(){
    if(this.passEntry !== this.user.lozinka){
      this.dialog.open(InformationDialogComponent, {
        data: 'Unesena lozinka je pogresna',
        disableClose: true,
        autoFocus: false
      });
      
      this.passEntry = "";
    } else if(this.newPassEntry1 !== this.newPassEntry2){
      this.dialog.open(InformationDialogComponent, {
        data: 'Nova lozinka i ponovljena lozinka su nejednake',
        disableClose: true,
        autoFocus: false
      });

      this.newPassEntry1 = "";
      this.newPassEntry2 = "";
    } else {
      this.userService.changeUserPassword(this.user.indeks, this.passEntry, this.newPassEntry1).subscribe(
        (res) => {
          /**Potrebno je da promenimo vrednosti */
          this.user.lozinka = this.newPassEntry1;
          localStorage.setItem('currentUser', JSON.stringify(this.user));

          this.dialog.open(InformationDialogComponent, {
            data: 'Uspešno ste promenili lozinku',
            disableClose: true,
            autoFocus: false
          });

          /**Posle promene hocemo da se vratimo na nas nalog */
          this.pass_change_el = false;
        },
        (err)=>{
          this.dialog.open(InformationDialogComponent, {
            data: 'Promena lozinke nije uspela',
            disableClose: true,
            autoFocus: false
          });
         }
      );
    }
  }

  public deleteAccount(){
    this.deleteIsConfirmed = true;
    
    var checkBoxDelete = <HTMLInputElement>document.getElementById("confirmDelete");
    /**Ako je checkboks cekiran, saljemo http zahtev za brisanje naloga */
    if (checkBoxDelete.checked == true){
      this.userService.deleteUser(this.user.indeks).subscribe(
        (res) => {
          //logOut ce da uradi sve sto nam treba 
          this.logOut()

          this.dialog.open(InformationDialogComponent, {
            data: 'Nalog je obrisan',
            disableClose: true,
            autoFocus: false
          });
        }, (err) => {
          this.dialog.open(InformationDialogComponent, {
            data: 'Neuspešno brisanje naloga',
            disableClose: true,
            autoFocus: false
          });
        }  
      )};
  }

  scrollToElement(elId: string): void {
    this.router.navigate(['/home-page'], {fragment: elId});
  }

  public logOut(){
    localStorage.setItem('isLoggedIn', 'false');
    this.router.navigate(['/login']);
    localStorage.setItem('currentUser', "");
    //localStorage.removeItem('student');
  }

}
