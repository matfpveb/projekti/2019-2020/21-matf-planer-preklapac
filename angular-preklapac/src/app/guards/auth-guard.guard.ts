import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardGuard implements CanActivate {
  public constructor(
    public router: Router
  ) { }
  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
     if (this.isLoggedIn()) {
     return true;
     }
     // navigate to login page as user is not authenticated
     this.router.navigate(['/']);
     return false;
}
public isLoggedIn(): boolean {
  let status = false;
  if (localStorage.getItem('isLoggedIn') === 'true') {
     status = true;
  } else {
     status = false;
     }
  return status;
  }
}
