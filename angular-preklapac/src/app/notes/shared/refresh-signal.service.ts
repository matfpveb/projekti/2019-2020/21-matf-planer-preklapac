import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RefreshSignalService {
  private subject = new Subject<any>();

  sendSignal(sig: number){
    this.subject.next({signal : sig});
  }

  getSignal(): Observable<any>{
    return this.subject.asObservable();
  }

  constructor() { }
}
