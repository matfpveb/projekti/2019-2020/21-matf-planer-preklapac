import { TestBed } from '@angular/core/testing';

import { RememberIndexService } from './remember-index.service';

describe('RememberIndexService', () => {
  let service: RememberIndexService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RememberIndexService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
