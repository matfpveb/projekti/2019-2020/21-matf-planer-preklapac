import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FullCalendarModule } from '@fullcalendar/angular'; // the main connector. must go first
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin

import { FormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { LoginPageComponent } from './login-page/login-page.component';
import {ReactiveFormsModule} from '@angular/forms';
import {RasporedComponent} from './raspored/raspored.component';
import { AppRoutingModule } from './app-routing.module';
import { NavigationComponent } from './navigation/navigation.component';
import { LoginRegisterService } from './services/login-register-service.service';
import { HttpClientModule } from '@angular/common/http';
import { InformationDialogComponent } from './dialogs/information-dialog/information-dialog.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import { HomePageComponent } from './home-page/home-page.component';
import { MainLayoutComponent } from './notes/pages/main-layout/main-layout.component';
import { NoteListComponent } from './notes/pages/note-list/note-list.component';
import { NotecardComponent } from './notes/notecard/notecard.component';
import { NoteDetailsComponent } from './notes/pages/note-details/note-details.component';
import {KalendarComponent} from './kalendar/kalendar.component';

import { Calendar } from '@fullcalendar/core';
import interactionPlugin from '@fullcalendar/interaction';

FullCalendarModule.registerPlugins([ // register FullCalendar plugins
  dayGridPlugin,
  interactionPlugin
]);


import { ErrorPageComponent } from './error-page/error-page.component';
import { MyAccountComponent } from './my-account/my-account.component';
import { ExamGradeListComponent } from './exam-grade-list/exam-grade-list.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    RasporedComponent,
    NavigationComponent,
    InformationDialogComponent,
    HomePageComponent,
    KalendarComponent,
    MainLayoutComponent,
    NoteListComponent,
    NotecardComponent,
    NoteDetailsComponent,
    ErrorPageComponent,
    MyAccountComponent,
    ExamGradeListComponent,
  ],
  imports: [

    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatButtonModule,
    FullCalendarModule
  ],
  providers: [
   LoginRegisterService
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    InformationDialogComponent
  ]
})
export class AppModule { }
