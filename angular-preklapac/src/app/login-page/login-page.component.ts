import { strict } from 'assert';
import { InformationDialogComponent } from './../dialogs/information-dialog/information-dialog.component';
import { LoginRegisterService } from './../services/login-register-service.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from '../models/user';
import { Router } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, DialogPosition} from '@angular/material/dialog';
import { RememberIndexService } from '../services/remember-index.service';
import { NotesService } from '../notes/shared/notes.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  public loginForm: FormGroup;
  public registerForm : FormGroup;
  public proceed_registration = false;

  constructor(
    public rememberService :RememberIndexService,
    private formBuilder: FormBuilder,
    private loginRegisterService: LoginRegisterService,
    private router: Router,
    protected dialog: MatDialog,
    private notesService: NotesService
    ) {
    this.loginForm = this.formBuilder.group({
      //regex za indeks - oblika 1/2011, 21/2011, 213/2011 npr
      index: ['', [Validators.required, Validators.pattern('[0-9]{1,3}\/[0-9]{4}')]],
      password: ['', [Validators.required]]
    });

    this.registerForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.pattern('[A-Z][a-z]+')]],
      surname: ['', [Validators.required, Validators.pattern('[A-Z][a-z]+')]],
      //regex za indeks - oblika 1/2011, 21/2011, 213/2011 npr
      index: ['', [Validators.required, Validators.pattern('[0-9]{1,3}\/[0-9]{4}')]],
      password: ['', [Validators.required]]
    });


   }

  ngOnInit(): void {
  }


 //Validatori za formular REGISTRACIJE
  public get name(){
     return this.registerForm.get('name');
   }

  public get surname(){
     return this.registerForm.get('surname');
   }

  public get index(){
    return this.registerForm.get('index');
  }

  public get password(){
    return this.registerForm.get('password');
  }


  //Validatori za formular LOGOVANJA

  public indexLog(){
    return this.loginForm.get('index');
  }

  public passwordLog(){
    return this.loginForm.get('password');
  }

  public submitLoginForm(): void{
    if(!this.loginForm.valid){
      window.alert('Nisu popunjena sva polja / Sva polja nisu validna!');
      return;
    }

    this.loginRegisterService.getUserByIndex(this.loginForm.controls.index.value, this.loginForm.controls.password.value).subscribe(
       (user: User) => {
          let currentUser = {
            ime: user.ime,
            prezime: user.prezime,
            indeks : user.indeks,
            lozinka: this.loginForm.controls.password.value
          };
          //kako bi podaci o studentu bili vidljivi na nivou homepage stranice
          localStorage.setItem('currentUser', JSON.stringify(currentUser));
          localStorage.setItem('student', user.indeks);

          localStorage.setItem('isLoggedIn', 'true');
         
          /**Kako bi se NoteList refreshovala.. */
          this.notesService.refreshNotes();

          this.router.navigate(['/home-page']);
          this.dialog.open(InformationDialogComponent, {
            data: 'Uspešno ste se prijavili',
            disableClose: true,
            autoFocus: false
          });
          this.rememberService.setindex(this.loginForm.controls.index.value);
          },
          (err)=>{
          this.dialog.open(InformationDialogComponent, {
            data: 'Pogrešan broj indeksa ili lozinka',
            disableClose: true,
            autoFocus: false
          });
         }
         
       
     );

  }

  public registerStudent(): void{

    if(!this.registerForm.valid){
      window.alert('Nisu popunjena sva polja / Sva polja nisu validna!');
      return;
    }
    const user: User =  {
      indeks: this.registerForm.controls.index.value,
      lozinka:this.registerForm.controls.password.value,
      ime: this.registerForm.controls.name.value,
      prezime: this.registerForm.controls.surname.value    };

    let currentUser = {
      ime: user.ime,
      prezime: user.prezime,
      indeks : user.indeks,
      lozinka: this.registerForm.controls.password.value
    };

    this.loginRegisterService.registerUser(user).subscribe(
      (res) => {
        localStorage.setItem('student', user.indeks);
        //kako bi podaci o studentu bili vidljivi na nivou homepage stranice
        localStorage.setItem('currentUser', JSON.stringify(currentUser));
        localStorage.setItem('isLoggedIn', 'true');
        
        /**Kako bi se NoteList refreshovala.. */
        this.notesService.refreshNotes();


        this.dialog.open(InformationDialogComponent, {
          data: 'Uspešno ste se registrovali',
          disableClose: true,
          autoFocus: false
        });
        this.rememberService.setindex(this.loginForm.controls.index.value);
        this.router.navigate(['/home-page']);
      },
      (err) => {
        console.log(err);
        this.dialog.open(InformationDialogComponent, {
          data: 'Došlo je do greške!',
          disableClose: true,
          autoFocus: false
        });
      }
    );
  }

  public submitForm(data){
    
  }
}
