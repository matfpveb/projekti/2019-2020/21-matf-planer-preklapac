import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpErrorHandler } from 'src/app/services/httpErrorHandler.model';
import {ExamGrade} from './examGrade.model';

@Injectable({
  providedIn: 'root'
})
export class ExamGradeService extends HttpErrorHandler{
   //sad ce biti tok a ne niz
   private exams: Observable<ExamGrade[]>;

   private readonly examUrl = "http://localhost:3004/examGrade/";
 
   //indeks tekuceg ulogovanog studenta
   private index;
 
   constructor(private http: HttpClient, router: Router) {
     super(router);
 
     if(localStorage.getItem('isLoggedIn') === "true"){
       this.refreshExams();
     }
   }
 
   refreshExams(): Observable<ExamGrade[]>{
     if(localStorage.getItem('isLoggedIn') === "true"){
       this.index = JSON.parse(localStorage.getItem('currentUser')).indeks;
     }
     //podesavanje parametra zahteva
     let params = new HttpParams().set('index', this.index);
 
     this.exams =  this.http.get<ExamGrade[]>(this.examUrl , {'params': params})
     .pipe(
       catchError(super.handleError())
     );
     
     return this.exams;
   }
 
   getAllExams(): Observable<ExamGrade[]>{
     return this.exams;
   }

  addExam(subject: string, test: number, exam: number){
    const body = {
      index: this.index,
      subject: subject,
      testPoints: test,
      examPoints: exam
    };
    return this.http.post<ExamGrade>(this.examUrl + 'new_exam', body)
    .pipe(
      catchError(super.handleError())
    );
    
  }

  deleteExam(exam_id: string){
    let params = new HttpParams()
    .set('exam_id', exam_id);
    return this.http.delete(this.examUrl + 'delete_exam', {'params': params})
    .pipe(
      catchError(super.handleError())
    );
  }

  updateExam(exam_id: string, testPoints: number, examPoints: number){
    let body = {
      index: this.index,
      testPoints: testPoints,
      examPoints: examPoints,
      exam_id: exam_id
    }
    return this.http.post<ExamGrade>(this.examUrl + 'update_exam', body)
    .pipe(
      catchError(super.handleError())
    );
  }
 
 
}
