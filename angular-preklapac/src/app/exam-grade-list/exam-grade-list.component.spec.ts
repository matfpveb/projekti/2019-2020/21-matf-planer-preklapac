import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamGradeListComponent } from './exam-grade-list.component';

describe('ExamGradeListComponent', () => {
  let component: ExamGradeListComponent;
  let fixture: ComponentFixture<ExamGradeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamGradeListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamGradeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
